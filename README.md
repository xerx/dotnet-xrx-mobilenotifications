# dotnet-xrx-mobilenotifications

Back end mobile notifications service

## FCM

#### Initialize your service
```c#
INotificationService service = new FirebaseNotificationService("[FIREBASE_PROJECT_ID]");
```

#### Create a message
```c#
INotificationMessage message = new FirebaseMessage
{
    Title = "A Title",
    Body = "A Body",
    Data = new Dictionary<string, object>
    {
        {"custom data", 123 }
    }
};
```

#### Send it to one or many devices
```c#
INotificationResponse response = await service.SendAsync(message, new List<string>
{
    "eaMz6-yn543aNvk:asA00P8sag_EQtqGmps8z19dCfjuUURSZmuDTejGsrP0OtmyVaCVzUNxSBOj6IhRGICmXlz",
    "eaMz6-n4aNv1nfk:as7APsa87g_EQtqGmps8z19dCfjuUURSZmuDTejGsrP0OtmyVaCVzUNxSBOj6IhRGICmXlz"
});
```

#### Handle the response
```c#
Console.WriteLine($"Success: {response.Success}");
Console.WriteLine($"Info: {response.Info}");
if(response.Success)
{
    foreach (var res in response.DeviceResponses)
    {
        Console.WriteLine($"_________________________");
        Console.WriteLine($"Info: {res.Info}");
        Console.WriteLine($"Success: {res.Success}");
    }
}
```

            
