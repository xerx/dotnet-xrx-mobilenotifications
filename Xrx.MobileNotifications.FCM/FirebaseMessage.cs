﻿using System.Collections.Generic;

namespace Xrx.MobileNotifications.FCM
{
    public class FirebaseMessage : NotificationMessage
    {
        private Dictionary<string, object> data;
        override public Dictionary<string, object> Data
        {
            set { data = value; }
            get
            {
                if (data != null)
                {
                    data["title"] = Title;
                    data["body"] = Body;
                    data["badge"] = Badge;
                }
                return data;
            }
        }
    }
}
