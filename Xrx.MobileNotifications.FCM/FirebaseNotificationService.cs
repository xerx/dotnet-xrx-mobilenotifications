﻿using FCM.Net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Xrx.MobileNotifications.Abstractions;

namespace Xrx.MobileNotifications.FCM
{
    //Firebase Cloud Messaging is free of charge, no matter the number of users and message pushes,
    //just like Google Cloud Messaging was.However, here are some of its product "limitation": 
    //There's a limit of 1000 registration tokens used when using registration_ids parameter per send.
    //So if you want to send 5000 messages to different users, you'll need to send the message in 5 batches.
    //There's a limit of 100 messages that can be stored without collapsing. You can read more details here.
    //You may also encounter limitations such as sending too much messages per second. Aside from that,
    //everything in FCM is free and unlimited.
    //We currently don't have an SLA for FCM since it's a free service. We could never guarantee the delivery
    //of messages since we do not control the networks between our servers and the device. We also offer the
    //same level of support for both our paid and free plans for this service.Kindly see our Pricing FAQ
    //here, under "What kind of support will I receive?" for more information.
    //You can get the server Key by accessing the url
    //https://console.firebase.google.com/project/MY_PROJECT/settings/cloudmessaging";
    //Message payload must contain notification property when sent in iOS
    //Errors https://firebase.google.com/docs/cloud-messaging/http-server-ref#table9
    public class FirebaseNotificationService : INotificationService
    {
        private readonly string projectServerKey;
        public FirebaseNotificationService(string projectServerKey)
        {
            this.projectServerKey = projectServerKey;
        }

        public Task<INotificationResponse> SendAsync(INotificationMessage message, string deviceRegistrationId)
        {
            return SendFcmMessageAsync(new Message
            {
                Notification = new Notification
                {
                    Title = message.Title,
                    Body = message.Body,
                    Sound = message.Sound,
                    Badge = message.Badge
                },
                To = deviceRegistrationId,
                Data = message?.Data
            });
        }

        public Task<INotificationResponse> SendAsync(INotificationMessage message, List<string> deviceRegistrationIds)
        {
            return SendFcmMessageAsync(new Message
            {
                Notification = new Notification
                {
                    Title = message.Title,
                    Body = message.Body,
                    Sound = message.Sound,
                    Badge = message.Badge
                },
                RegistrationIds = deviceRegistrationIds,
                Data = message?.Data
            });
        }
        private async Task<INotificationResponse> SendFcmMessageAsync(Message fcmMessage)
        {
            ResponseContent result = null;
            Exception exception = null;
            NotificationResponse response = new NotificationResponse();
            using (Sender sender = new Sender(projectServerKey))
            {
                try
                {
                    result = await sender.SendAsync(fcmMessage);
                }
                catch(Exception ex)
                {
                    exception = ex;
                }
            }
            if(exception == null)
            {
                response.Success = result.StatusCode == HttpStatusCode.OK;
                response.Info = result.ReasonPhrase;
                response.DeviceResponses = result.MessageResponse?.Results?
                    .Select(res => new DeviceResponse
                    {
                        Success = string.IsNullOrEmpty(res.Error),
                        Info = res.Error,
                        RegistrationId = res.RegistrationId
                    })
                    .ToList<IDeviceResponse>();
            }
            else
            {
                response.Info = exception.Message;
                response.Success = false;
            }
            return response;
        }
    }
    
}
