﻿using System;
using System.Collections.Generic;
using Xrx.MobileNotifications.Abstractions;
using Xrx.MobileNotifications.FCM;

namespace Playground
{
    class Program
    {
        static void Main(string[] args)
        {
            INotificationService service = new FirebaseNotificationService("[FIREBASE_PROJECT_URL]");
            INotificationMessage message = new FirebaseMessage
            {
                Title = "A Title",
                Body = "A Body",
                Data = new Dictionary<string, object>
                {
                    {"custom data", 123 }
                }
            };
            INotificationResponse r = service.SendAsync(message, new List<string>
                {
                    "eaMz6-yn543aNvk:asA00P8sag_EQtqGmps8z19dCfjuUURSZmuDTejGsrP0OtmyVaCVzUNxSBOj6IhRGICmXlz",
                    "eaMz6-n4aNv1nfk:as7APsa87g_EQtqGmps8z19dCfjuUURSZmuDTejGsrP0OtmyVaCVzUNxSBOj6IhRGICmXlz"
                })
                .GetAwaiter().GetResult();


            Console.WriteLine($"Success: {r.Success}");
            Console.WriteLine($"Info: {r.Info}");
            if(r.Success)
            {
                foreach (var re in r.DeviceResponses)
                {
                    Console.WriteLine($"_________________________");
                    Console.WriteLine($"Info: {re.Info}");
                    Console.WriteLine($"Success: {re.Success}");
                }
            }
            Console.ReadLine();
        }
    }
}
