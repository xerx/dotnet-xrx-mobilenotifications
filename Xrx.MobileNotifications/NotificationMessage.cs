﻿using Xrx.MobileNotifications.Abstractions;
using System.Collections.Generic;

namespace Xrx.MobileNotifications
{
    public class NotificationMessage : INotificationMessage
    {
        public virtual string Title { get; set; }
        public virtual string Body { get; set; }
        public virtual string Badge { get; set; } = "0";
        public virtual string Sound { get; set; } = "default";
        public virtual Dictionary<string, object> Data { get; set; }
    }
}
