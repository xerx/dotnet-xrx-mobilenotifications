﻿using Xrx.MobileNotifications.Abstractions;
using System.Collections.Generic;

namespace Xrx.MobileNotifications
{
    public class NotificationResponse : BaseResponse, INotificationResponse
    {
        public virtual List<IDeviceResponse> DeviceResponses { get; set; }
    }
}
