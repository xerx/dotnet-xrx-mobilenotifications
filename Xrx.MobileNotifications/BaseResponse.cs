﻿using Xrx.MobileNotifications.Abstractions;

namespace Xrx.MobileNotifications
{
    public class BaseResponse : IResponse
    {
        public virtual bool Success { get; set; }
        public virtual string Info { get; set; }
    }
}
