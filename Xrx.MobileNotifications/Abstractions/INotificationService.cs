﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace Xrx.MobileNotifications.Abstractions
{
    public interface INotificationService
    {
        Task<INotificationResponse> SendAsync(INotificationMessage message, string deviceId);
        Task<INotificationResponse> SendAsync(INotificationMessage message, List<string> deviceIds);
    }
}
