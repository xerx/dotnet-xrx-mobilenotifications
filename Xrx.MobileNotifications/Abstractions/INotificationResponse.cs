﻿using System.Collections.Generic;

namespace Xrx.MobileNotifications.Abstractions
{
    public interface INotificationResponse : IResponse
    {
        List<IDeviceResponse> DeviceResponses { get; set; }
    }
}