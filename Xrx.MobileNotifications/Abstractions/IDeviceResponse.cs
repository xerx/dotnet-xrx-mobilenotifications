﻿namespace Xrx.MobileNotifications.Abstractions
{
    public interface IDeviceResponse : IResponse
    {
        string RegistrationId { get; set; }
    }
}