﻿using System.Collections.Generic;

namespace Xrx.MobileNotifications.Abstractions
{
    public interface INotificationMessage
    {
        string Title { get; set; }
        string Body { get; set; }
        Dictionary<string, object> Data { get; set; }
        string Badge { get; set; }
        string Sound { get; set; }
    }
}