﻿namespace Xrx.MobileNotifications.Abstractions
{
    public interface IResponse
    {
        string Info { get; set; }
        bool Success { get; set; }
    }
}
