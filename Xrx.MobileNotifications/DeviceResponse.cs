﻿using Xrx.MobileNotifications.Abstractions;

namespace Xrx.MobileNotifications
{
    public class DeviceResponse : BaseResponse, IDeviceResponse
    {
        public virtual string RegistrationId { get; set; }
    }
}
